__author__ = 'ingared'

from Gen_imports import *
class k_means():

    """
    K - means algorithm for a give data set
    """

    def cluster_data(self,X,k):
        """

        :param k: no of clusters
        :return: clustered data set
        """
        shape = np.shape(X)

        y = self.initialize_cluster(X,k)
        print y
        #raw_input()
        clusters = self.calculate_mean(X,y,k)
        cluster_distances = 100.0
        iterations = 100
        count = 0
        z=[]
        while(cluster_distances > pow(10,-10) and count < iterations):
            #print "iterations",count
            y_updated =  self.calculate_min_distance_cluster(X,clusters)
            updated_clusters = self.calculate_mean(X,y_updated,k)
            cluster_distances = self.calculate_clusters_distances(clusters,updated_clusters)

            count += 1
            print cluster_distances
            clusters = updated_clusters
            z.append(updated_clusters[0])

        print count
        return y_updated,updated_clusters

    def initialize_cluster(self,X,k):
        """
        Randomly initializes the data in to k clusters
        :param X:
        :return:
        """
        return np.uint8(np.floor(k*np.random.rand(len(X))))

    def points_in_clusters(self,y,k):
        """
        :param y: cluster_index of points
        :param k: no of clusters
        :return:
        """
        y2 = y.copy()
        y1 = np.asarray([sum(y==x) for x in range(k)])
        while (min(y1)==0):
            z = np.argmin(y1)
            random_point = np.random.randint(0,len(y))
            #print z, 'Z',y2[random_point]
            y2[random_point] = z
            #print y2[random_point]
            y1 = np.asarray([sum(y2==x) for x in range(k)])
            #print y1
        print "ended"

        return y2

    def plot_clusters(self,X,clusters):
        plot(X[:,0],X[:,1],'b*')
        pylab.hold(True)
        plot(clusters[:,0],clusters[:,1],'r*',)
        show()


    def plot1(self,X,y,clusters):
        c = ['r','b','g']
        for i in range(3):
            X1 = (y==0)
            plot(X[X1,0],X[X1,1],c[i]+'*')
            pylab.hold(True)
            plot(clusters[:,0],clusters[:,1],c[i]+'o')
        show()

    def calculate_mean(self,X,y,k):

        """
        Calculates the k - means of the cluster
        :param X:
        :param y:
        :return: cluster -means

        """
        y1 = np.asarray([sum(y==x) for x in range(k)])
        if (min(y1) == 0):
            y = self.points_in_clusters(y,k)
        shape = np.shape(X)
        clusters = np.zeros((k,shape[1]))
        for c in range(k):
            clusters[c,:] =  np.mean(X[ (y == c),:],axis=0)
        return clusters


    def calculate_min_distance_cluster(self,X,clusters):

        """
        Calculate the distances from the
        :return:
        """

        distances = np.zeros((len(X),len(clusters)))
        for i,cluster in enumerate(clusters):
            distances[:,i] = np.sum((X - cluster)**2 , axis=1)
        return np.argmin(distances,axis=1)

    def calculate_clusters_distances(self,cluster1,cluster2):
        """

        :param cluster1:
        :param cluster2:
        :return: checks the change in movement of clusters
        """
        distance = ((cluster1-cluster2)**2)
        return np.sum(distance)


