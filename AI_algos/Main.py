__author__ = 'ingared'

from Gen_imports import *
from em import EM
from k_means import k_means

k = k_means()
y,u = k.cluster_data(xt,3)

k.plot_clusters(xt,u)
k.plot1(xt,y,u)
#pylab.hold()

#accuracy = np.sum((y+1) == (xtest))/float(len(y))
#print accuracy,"accuracy"

# Rand index is  fraction of sum of True positives and True negatives
d = 0
for i in range(len(y)):
    for j in range(i+1,len(y)):
        da = (y[i]==y[j])
        ba = (xtest[i]==xtest[j])
        if (da == ba):
            d += 1
            #print da, ba,
        else:
            pass
            #d += 1
n = len(y)
rand_indx = 2.0*d/(n*n -n)
print rand_indx,"rand_indx"

res = np.zeros(100)
for jj in range(100):
    k = k_means()
    y,u = k.cluster_data(xt,3)
    d = 0
    for i in range(len(y)):
        for j in range(i+1,len(y)):
            da = (y[i]==y[j])
            ba = (xtest[i]==xtest[j])
            if (da == ba):
                d += 1
    n = len(y)
    rand_indx = 2.0*d/(n*n -n)
    res[jj] = rand_indx

#k.plot_clusters(xt,u)

#e = EM()
#e.initialise_clusters(xt,3)
