__author__ = 'ingared'


import numpy as np
from Gen_imports import *

def initialze_Param(data,nclus):

    n,m = np.size(data,0),np.size(data,1)
    meanVect = np.zeros((nclus,m))
    covMat = np.zeros((nclus,m,m))
    coefVect = np.zeros((nclus,1))

    cluster = np.ceil(3*np.random.rand(n))-1

    for i in range(nclus):
        meanVect[i,:] = np.mean(data[cluster == i,:], axis=0)
        covMat[i,:,:] = np.cov(data[cluster == i,:].T)
        coefVect[i] = np.sum(cluster == i)/float(n)

    return meanVect, covMat, coefVect

def multiVarGauss(x,m,c):
        d = np.size(m)
        detC = np.linalg.det(c)
        return (1/np.sqrt(np.power(2*np.pi,d)*detC))*np.exp(-0.5* np.dot(np.dot((x - m),np.linalg.inv(c)),(x-m).T))


def Expectation(data,gamma,m,c,coef,numc,n):
    for i in range(n):
        temp = 0
        for k in range(numc):
            gamma[i,k] = coef[k]*multiVarGauss(data[i,:], m[k,:],c[k,:,:])
            temp += gamma[i,k]
        gamma[i,:] = gamma[i,:]/temp

    return gamma

def Maximization(data,gamma,m,c,numc,n):
    Nk = np.sum(gamma,axis=0)
    m = np.zeros([np.size(m,axis=0),np.size(m,axis=1)])
    c = np.zeros([np.size(c,axis=0),np.size(c,axis=1),np.size(c,axis=2)])

    # Mean and Covariance Matrix update
    for i in range(n):
        for k in range(numc):
            m[k,:] += (1/Nk[k])*gamma[i,k]*data[i,:]

    for i in range(n):
        for k in range(numc):
            c[k,:,:] += (1/Nk[k])*gamma[i,k]*((np.matrix(data[i,:] - m[k,:])).T*(data[i,:]-m[k,:]))

    coef = Nk/n

    return m,c,coef


def EM_GM(data, numc):
    m,c,coef = initialze_Param(data,numc)
    n = np.size(data,axis=0)
    mlVal = 0
    converged = False
    label = np.zeros(n)
    iter = 0
    while not converged:

        # Expectation Step
        gamma = np.zeros([n, numc])
        gamma = Expectation(data,gamma,m,c,coef,numc,n)

        label = np.argmax(gamma,axis=1)
        # Maximization Step
        m,c,coef = Maximization(data,gamma,m,c,numc,n)

        mlValNew = 0

        for i in range(n):
            temp = 0
            for k in range(numc):
                temp += coef[k]*multiVarGauss(data[i,:],m[k,:],c[k,:,:])
            mlValNew += np.log(temp)
        if np.abs(mlValNew - mlVal) < 1e-10 or iter > 2000:
            converged = True
        mlVal = mlValNew
        iter += 1
    print "Converged in ", iter
    return label


y = EM_GM(xt,3)

d = 0
for i in range(len(y)):
    for j in range(i+1,len(y)):
        da = (y[i]==y[j])
        ba = (xtest[i]==xtest[j])
        if (da == ba):
            d += 1
n = len(y)
rand_indx = 2.0*d/(n*n -n)
print rand_indx,"rand_indx"

res = np.zeros(100)
for jj in range(100):
    y = EM_GM(xt,3)
    d = 0
    for i in range(len(y)):
        for j in range(i+1,len(y)):
            da = (y[i]==y[j])
            ba = (xtest[i]==xtest[j])
            if (da == ba):
                d += 1
    n = len(y)
    rand_indx = 2.0*d/(n*n -n)
    res[jj] = rand_indx
