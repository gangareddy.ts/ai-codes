__author__ = 'ingared'


import numpy as np

def initialze_Param(data,nclus):

    n,m = np.size(data,0),np.size(data,1)
    meanVect = np.zeros((nclus,m))
    covMat = np.zeros((nclus,m,m))
    coefVect = np.zeros((nclus,1))

    cluster = np.ceil(3*np.random.rand(n))-1

    for i in range(nclus):
        meanVect[i,:] = np.mean(data[cluster == i,:], axis=0)
        covMat[i,:,:] = np.cov(data[cluster == i,:].T)
        coefVect[i] = np.sum(cluster == i)/float(n)

    return meanVect, covMat, coefVect