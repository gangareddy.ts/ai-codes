__author__ = 'ingared'

from Gen_imports import *

class Normal():

    """
    Normal distribution
    """

    def find_params(self,X):

        """
        finds mean and covariances of the given data set
        :return:
        """

        mean = np.mean(X,axis =1)
        covar = np.cov(X.T)

        return mean,covar

    def find_prob(self,X,mean,covar):
        """
        returns the probability density of x.
        :param x:
        :param mean:
        :param covar:
        :return:
        """
        print "covar", covar
        z = np.zeros(len(X))
        pi = np.pi
        d = len(mean)
        det1 = np.linalg.inv(covar)
        det2 = np.linalg.det(det1)
        e = np.e
        xx = X - mean
        const = det2/pow(2*pi,d/2.0)
        for i,x in enumerate(xx):
            v = np.sum(np.dot(x,det1)*x)
            z[i] = const*pow(e,-v)
        return z

    def norm_pdf_multivariate(self,X, mu, sigma):
        z = np.zeros(len(X))
        for i,x in enumerate(X):
            size = len(x)
            if size == len(mu) and (size, size) == sigma.shape:
                det = np.linalg.det(sigma)
                if det == 0:
                    raise NameError("The covariance matrix can't be singular")
                norm_const = 1.0/ ( np.math.pow((2*np.pi),float(size)/2) * np.math.pow(det,1.0/2) )
                x_mu = np.matrix(x - mu)
                inv = sigma*np.matrix(np.identity(len(mu)))
                result = np.math.pow(np.math.e, -0.5 * (x_mu * inv * x_mu.T))
                z[i] = norm_const * result
            else:
                    raise NameError("The dimensions of the input don't match")
        return z



