__author__ = 'ingared'

from Gen_imports import *
from k_means import k_means
from normal import Normal


class EM():
    """
    Expectation - maximization Algorithm for Mixture of Gaussians.
    """

    def initialise_clusters(self,X,k):

        """
          the values using  hard clustering
        i.e using k - means
        :return: the initialized values
        """
        k_m = k_means()
        y,clusters = k_m.cluster_data(X,k)
        cluster_count,cluster_mean, cluster_var = self.calculate_cluster_mean_variance(X,y,clusters)
        print cluster_mean,"mean"
        convergence = False

        param_diff = 100
        iterations = 0
        while param_diff > 0.001 and iterations < 10 :

            D = self.expectation(X,cluster_count,clusters,cluster_var)
            cluster_count,clusters_updated,cluster_var_updated = self.maximisation(D,X)
            print clusters_updated,"updated"
            iterations += 1
            param_diff_means = np.sum(abs(clusters_updated - clusters))
            param_diff_var = np.sum(abs(cluster_var - cluster_var_updated))
            clusters = clusters_updated
            cluster_var = cluster_var_updated
            print clusters,"clusters"
            #print iterations,param_diff_means, param_diff_var



    def calculate_cluster_mean_variance(self,X,y,clusters):
        """

        :param X:
        :param y:
        :param clusters:
        :return:
        """
        d = len(clusters[0])
        cluster_mean = np.zeros((len(clusters),d))
        cluster_var = np.zeros((len(clusters),d,d))
        cluster_count = np.asarray([sum(y==j) for j in range(len(clusters))])

        for i in range(len(clusters)):
            X1 =  X[(y == i),:]
            cluster_mean[i,:] = np.mean(X1,axis=0)
            cluster_var[i,:] = np.cov(X1,None,rowvar=0)
        return cluster_count/sum(cluster_count+0.0),cluster_mean,cluster_var

    def expectation1(self,X,y,clusters):
        """
        Expectation step
        :return:
        """
        D = np.zeros((len(X),len(clusters)))
        cluster_count,cluster_mean, cluster_var = self.calculate_cluster_mean_variance(X,y,clusters)

        for i in range(len(X)):
            for j,cluster in enumerate(clusters):
                D[i,j] = cluster_count[j]*Normal.find_prob(X[i,:],cluster_mean[j],cluster_var[j])
        sum_factor = np.repeat(1.0/np.sum(D,axis = 1),len(clusters))
        sumfactor = np.reshape(sum_factor,np.shape(D))
        return D*sumfactor

    def expectation(self,X,cluster_count,clusters, cluster_var):
        """
        Expectation step
        :return:
        """
        D = np.zeros((len(X),len(clusters)))
        norm = Normal()
        #for i in range(len(X)):
        for j,cluster in enumerate(clusters):
            print clusters[j],cluster_var[j],"dvsds"
            #pp = norm.find_prob(X,clusters[j],cluster_var[j])
            pp = norm.norm_pdf_multivariate(X,clusters[j],cluster_var[j])
            D[:,j] = cluster_count[j]*pp
        sum_factor = np.repeat(np.sum(D,axis = 1),len(clusters))
        sumfactor = np.reshape(sum_factor,np.shape(D))
        #print sumfactor,"Sumfactor"
        return D*sumfactor

    def covar2(self,d,X,cluster_mean):
        """

        :return:
        """
        dimensions = np.shape(X)[1]
        z = np.zeros((dimensions,dimensions))
        for i in range(len(X)):
            z += d[i]*np.dot(X[i]- cluster_mean,(X[i]-cluster_mean).T)

        return z

    def maximisation(self,D,X):
        """
        Maximization step
        :return:
        """
        m,n = shape = np.shape(D)[0:2]
        no_of_points, dimensions = np.shape(X)[0:2]
        cluster_mean = np.zeros((n,dimensions))
        cluster_var = np.zeros((n,dimensions,dimensions))
        cluster_count = np.zeros(n)

        for j in range(n):
            cluster_count[j] = np.sum(D[:,j])
            D1 = np.reshape(np.repeat(D[:,j],dimensions),np.shape(X))
            cluster_mean[j,:] = np.sum(X*D1,axis=0) / cluster_count[j]
            cluster_var[j,:] = self.covar2(D[:,j],X,cluster_mean[j,:])

        return cluster_count/(0.0 + sum(cluster_count)),cluster_mean,cluster_var


#e = EM()
#e.initialise_clusters(xt,3)